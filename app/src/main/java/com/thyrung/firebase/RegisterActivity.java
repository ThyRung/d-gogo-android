package com.thyrung.firebase;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = "RegisterActivity";
    private EditText edEmail,edPassword;
    private Button btnRegister;
    private TextView txtLogin;
//    private ImageButton btnBackRegister;

    ProgressDialog progressDialog;
    private FirebaseAuth mAuth;

//    FirebaseDatabase database;
//    DatabaseReference myRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setTitle("Register");
//
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setDisplayShowCustomEnabled(true);


        edEmail = findViewById(R.id.edEmail_register);
        edPassword = findViewById(R.id.edPassword_register);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Register ...");

//  Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        txtLogin = findViewById(R.id.txt_login);
        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

////                Loading
//                Thread welcomeThread = new Thread() {
//                    @Override
//                    public void run() {
//                        try {
//                            super.run();
//                            sleep(10000);  //Delay of 10 seconds
//                        } catch (Exception e) {
//
//                        } finally {
//
//                            Intent i = new Intent(RegisterActivity.this,
//                                    LoginActivity.class);
//                            startActivity(i);
//                            finish();
//                        }
//                    }
//                };
//                welcomeThread.start();

                startActivity(new Intent(getApplicationContext(),LoginActivity.class));
            }
        });

        btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = edEmail.getText().toString().trim();
                String password = edPassword.getText().toString().trim();

//                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
//
//                    edEmail.setError("Invalid Email.");
//                    edEmail.setFocusable(true);
//
//                } else
                if (password.length() < 6) {

                    edPassword.setError("Password length least 6 character.");
                    edPassword.setFocusable(true);

                } else {
                    registerUser(email, password);
                }
            }
        });
    }

    private void registerUser(String email, String password) {
        progressDialog.show();
        Log.d(TAG, "registerUser: email "+email+", pass "+password);
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "Register :success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                            startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "Register :failure", task.getException());
                            Toast.makeText(getApplicationContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                        progressDialog.dismiss();

                    }
                });
    }
//  Check if user is signed in (non-null) and update UI accordingly
    private void updateUI(FirebaseUser user) {
        if (user != null) {
            Log.d(TAG, "updateUI: email : " + user.getEmail() + "\n uID : " + user.getUid());
        } else {
            Log.d(TAG, "updateUI: NULL ");
        }
    }
}