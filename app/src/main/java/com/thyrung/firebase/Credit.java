package com.thyrung.firebase;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

public class Credit extends AppCompatActivity {
    private ImageButton imagecredit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.credit);

        imagecredit = findViewById(R.id.close_credit);
        imagecredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.id_buttoncredit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDialogCredit();
            }
        });

    }
    private void ShowDialogCredit(){
        AlertDialog.Builder builder = new AlertDialog.Builder(Credit.this, R.style.AlertDailogTheme);
        View view = LayoutInflater.from(Credit.this).inflate(
                R.layout.dailog,(ConstraintLayout)findViewById(R.id.layoutDialogContainer)
        );
        builder.setView(view);
        ((TextView)view.findViewById(R.id.id_titlecredit)).setText(getResources().getString(R.string.dialog_title));
        ((TextView)view.findViewById(R.id.id_messagecredit)).setText(getResources().getString(R.string.dialog_Message));
        ((Button)view.findViewById(R.id.id_ok)).setText(getResources().getString(R.string.dialog_button));

        final AlertDialog alertDialog = builder.create();
        view.findViewById(R.id.id_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        if(alertDialog.getWindow()!= null){
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }
        alertDialog.show();
    }
//        public void ShowDialog (View view){
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setTitle("Add Credit");
//            builder.setMessage("At the moment, you can only top-up your credit at the office. " +
//                    "Please contact company administrator for more information.");
//            builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    Toast.makeText(getApplicationContext(), "Ok was Clicked", Toast.LENGTH_SHORT).show();
//                }
//            });
//            builder.create().show();
//        }v
}
