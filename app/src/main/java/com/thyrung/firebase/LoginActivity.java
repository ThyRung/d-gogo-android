package com.thyrung.firebase;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import static com.thyrung.firebase.R.id.bottom;
import static com.thyrung.firebase.R.id.hide_show_password;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    private EditText emailEd,passwordEd;
    private Button btnLogin;
    private TextView create_accountEd;
    private ImageView btnVisibilityOffOn;

    private RelativeLayout relativeLayout;
    private boolean showPassword = false;

    ProgressDialog progressDialog;
    private FirebaseAuth mAuth;

//    FirebaseDatabase database;
//    DatabaseReference myRef;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setTitle("Login");
//
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setDisplayShowCustomEnabled(true);

        emailEd = findViewById(R.id.edEmail_login);
        passwordEd = findViewById(R.id.edPassword_login);
        btnLogin = findViewById(R.id.btnLogin);

        btnVisibilityOffOn = findViewById(R.id.hide_show_password);
        relativeLayout = findViewById(R.id.relativeLayout_showHide_password);

        create_accountEd = findViewById(R.id.txt_register);
        create_accountEd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

////                Loading
//                Thread welcomeThread = new Thread() {
//                    @Override
//                    public void run() {
//                        try {
//                            super.run();
//                            sleep(10000);  //Delay of 10 seconds
//                        } catch (Exception e) {
//
//                        } finally {
//
//                            Intent i = new Intent(LoginActivity.this,
//                                    RegisterActivity.class);
//                            startActivity(i);
//                            finish();
//                        }
//                    }
//                };
//                welcomeThread.start();

                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Login ...");

//  Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = emailEd.getText().toString().trim();
                String password = passwordEd.getText().toString().trim();

//                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
//
//                    emailEd.setError("Invalid Email.");
//                    emailEd.setFocusable(true);
//                } else
                if (password.length() < 6) {
                    passwordEd.setError("Password length least 6 character.");
                    passwordEd.setFocusable(true);

                } else {
                    loginUser(email, password);
                }
            }
        });
    }

    private void loginUser(final String email, final String password) {
        progressDialog.show();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "Login :success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                            startActivity(new Intent(LoginActivity.this,MainActivity.class));
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "Login :failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                        progressDialog.dismiss();

                    }
                });
        // loginData();
    }

//  Check if user is signed in (non-null) and update UI accordingly
    private void updateUI(FirebaseUser user) {
        if (user != null) {
            Log.d(TAG, "updateUI: email : " + user.getEmail() + "\n uID : " + user.getUid());
        } else {
            Log.d(TAG, "updateUI: NULL ");
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed(); = Finish
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to Exit?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}