//package com.thyrung.firebase;
//
//import androidx.annotation.NonNull;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//import android.os.Bundle;
//
//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//import com.google.firebase.database.ValueEventListener;
//import com.thyrung.firebase.adapter.AdapterItem;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class DisplayActivity extends AppCompatActivity {
//
//    private RecyclerView recyclerView;
//    private AdapterItem adapterItem;
//    private List<DataItem> items;
//    private DatabaseReference databaseReference;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_display);
//
//        recyclerView = findViewById(R.id.list_view);
//        recyclerView.setHasFixedSize(true);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
//        recyclerView.setLayoutManager(linearLayoutManager);
//
//        // Declare List
//        items = new ArrayList<>();
//
//        DisplayData();
//    }
//
//// Button Display
//    private void DisplayData() {
//
//        databaseReference = FirebaseDatabase.getInstance().getReference("Data");
//        databaseReference.addValueEventListener(new ValueEventListener() {
//
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                items.clear();
//                for (DataSnapshot snapshot: dataSnapshot.getChildren())
//                {
//                    DataItem dataItem = snapshot.getValue(DataItem.class);
//                    items.add(dataItem);
//                }
//
//                adapterItem = new AdapterItem(getApplicationContext(),items);
//                recyclerView.setAdapter(adapterItem);
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//    }
//}