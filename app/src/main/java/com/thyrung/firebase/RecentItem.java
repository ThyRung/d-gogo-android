package com.thyrung.firebase;

import android.graphics.drawable.Drawable;

public class RecentItem {
    private Drawable icon1;
    private String day,month,year,time,total;

    public RecentItem(){
    }
    public RecentItem(Drawable icon1,String day,String month,
        String year,String time, String total){
        this.icon1 = icon1;
        this.day = day;
        this.month = month;
        this.year = year;
        this.time = time;
        this.total = total;
    }
    public Drawable getIcon1(){return icon1;}
    public void setIcon1(Drawable icon1){this.icon1 = icon1;}

    public String getDay(){return day;}
    public void setDay(String day){this.day = day;}

    public String getMonth(){return month;}
    public void setMonth(String month){this.month = month;}

    public String getYear(){return year;}
    public void setYear(String year){this.year = year;}

    public String getTime(){return time;}
    public void setTime(String time){this.time = time;}

    public String getTotal(){return total;}
    public void setTotal(String total){this.total = total;}

}
