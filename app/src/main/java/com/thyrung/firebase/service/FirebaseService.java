package com.thyrung.firebase.service;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.webkit.MimeTypeMap;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.thyrung.firebase.ProfileX;

import java.util.HashMap;
import java.util.Objects;

public class FirebaseService {

    private FirebaseFirestore firebaseFirestore;
    private FirebaseUser firebaseUser;
    private TextView tvName, tvPhone, tvCity;
    private CircularImageView imgGallery;

    private Context context;
    public FirebaseService(Context context) {

        this.context = context;
    }

    public interface OnCallBack{

        void onUploadSuccess(String imageUrl);
        void onUploadFailed(Exception e);
    }

    private String getFileExtention(Uri uri) {

        ContentResolver contentResolver = context.getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    public void uploadImageToFireBaseStorage(Uri uri, final OnCallBack onCallBack){

        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        final ProgressDialog progressDialog = new ProgressDialog(context);

//        show dialog
        progressDialog.setMessage("Loading...");
        progressDialog.show();

//        update to firebase
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Images/" + System.currentTimeMillis()+"."+getFileExtention(uri));
        storageReference.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                while (!urlTask.isSuccessful());
                Uri downloadUrl = urlTask.getResult();

                final String sdownload_url = String.valueOf(downloadUrl);

                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("imageProfile", sdownload_url);

                progressDialog.dismiss();
                onCallBack.onUploadSuccess(sdownload_url);

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                progressDialog.dismiss();
                onCallBack.onUploadFailed(e);

            }
        });
    }
}
