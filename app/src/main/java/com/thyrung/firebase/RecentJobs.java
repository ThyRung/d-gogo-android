package com.thyrung.firebase;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

public class RecentJobs extends AppCompatActivity {
    private List<RecentItem> list;
    private RecentJobsAdapter adapter;
    private RecyclerView recyclerView;
    private ImageButton cancelrecent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recent_jos);
        recyclerView = findViewById(R.id.recycler_view);
        cancelrecent = findViewById(R.id.cancelrecent);
        cancelrecent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        list = new ArrayList<>();

        list.add(new RecentItem(getResources().getDrawable(R.drawable.ic_baseline_not_listed_location_24),
                "Monday,","June 01,","2020","9:00","5,000.00 KHR"));
        list.add(new RecentItem(getResources().getDrawable(R.drawable.ic_baseline_not_listed_location_24),
                "Thuresday,","June 02,","2020","9:10","10,000.00 KHR"));
        list.add(new RecentItem(getResources().getDrawable(R.drawable.ic_baseline_not_listed_location_24),
                "Friday,", "June 13,", "2020", "9:40", "9,000.00 KHR"));
        list.add(new RecentItem(getResources().getDrawable(R.drawable.ic_baseline_not_listed_location_24),
                "Sunday,","June 24,","2020","9:57","7,000.00 KHR"));

       adapter = new RecentJobsAdapter(list, RecentJobs.this);
       recyclerView.setLayoutManager(new LinearLayoutManager(this));
       recyclerView.setAdapter(adapter);
    }
}
