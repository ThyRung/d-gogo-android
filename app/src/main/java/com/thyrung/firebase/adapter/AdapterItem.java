package com.thyrung.firebase.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.thyrung.firebase.R;
import com.thyrung.firebase.model.DataItem;

import java.util.List;

public class AdapterItem extends RecyclerView.Adapter<AdapterItem.ViewHolder> {

    private Context context;
    private List<DataItem> item;

    public AdapterItem(Context context, List<DataItem> item) {
        this.context = context;
        this.item = item;
    }
    @NonNull
    @Override
    public AdapterItem.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.activity_login,parent,false);
        return new AdapterItem.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final DataItem dataItem = item.get(position);
        holder.ed_Email.setText(dataItem.getEmail());
        holder.ed_password.setText(dataItem.getPassword());

    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private EditText ed_Email,ed_password;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ed_Email = itemView.findViewById(R.id.edEmail_login);
            ed_password = itemView.findViewById(R.id.edPassword_login);
        }
    }
}
