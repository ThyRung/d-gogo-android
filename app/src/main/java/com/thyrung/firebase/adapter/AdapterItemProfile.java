package com.thyrung.firebase.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.thyrung.firebase.R;
import com.thyrung.firebase.model.ProfileItem;

import java.util.List;

public class AdapterItemProfile extends RecyclerView.Adapter<AdapterItemProfile.ViewHolder> {

    private Context context;
    private List<ProfileItem> profileItems;

    public AdapterItemProfile(Context context, List<ProfileItem> profileItems) {
        this.context = context;
        this.profileItems = profileItems;
    }

    @NonNull
    @Override
    public AdapterItemProfile.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.profile_item,parent,false);
        return new AdapterItemProfile.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final ProfileItem profileItem = profileItems.get(position);
        holder.ciProfile.setImageDrawable(profileItem.getImageUrl());
        holder.tId.setText(profileItem.getId());
        holder.tName.setText(profileItem.getName());
        holder.tPhone.setText(profileItem.getPhone());
        holder.tCity.setText(profileItem.getCity());

    }

    @Override
    public int getItemCount() {
        return profileItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tId,tName,tPhone,tCity;
        private CircularImageView ciProfile;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tId = itemView.findViewById(R.id.mId);
            tName = itemView.findViewById(R.id.userName);
            tPhone = itemView.findViewById(R.id.mPhone);
            tCity = itemView.findViewById(R.id.mCity);
            ciProfile = itemView.findViewById(R.id.image_profile);
        }
    }
}
