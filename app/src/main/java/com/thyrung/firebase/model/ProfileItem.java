package com.thyrung.firebase.model;

import android.graphics.drawable.Drawable;

public class ProfileItem {

    Drawable imageUrl;
    String id;
    String name;
    String phone;
    String city;

    public ProfileItem(){}

    public ProfileItem(Drawable imageUrl, String id, String name, String phone, String city) {
        this.imageUrl = imageUrl;
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.city = city;
    }

    public Drawable getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(Drawable imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
