package com.thyrung.firebase.model;

import android.graphics.drawable.Drawable;

public class TranItem {
    private String percentorder, datetimer, timeorder, amountorder;

    public TranItem() {
    }

    public TranItem(String percentorder, String datetimer,
                    String timeorder, String amountorder) {
        this.percentorder = percentorder;
        this.datetimer = datetimer;
        this.timeorder = timeorder;
        this.amountorder = amountorder;
    }
    public void setPercentorder(String percentorder) { this.percentorder = percentorder; }
    public String getpercentorder() { return percentorder; }

    public void setDatetimer(String datetimer) { this.datetimer = datetimer; }
    public String getDatetimer() { return datetimer; }

    public void setTimeorder(String timeorder) { this.timeorder = timeorder; }
    public String getTimeorder() { return timeorder; }

    public void setAmountorder(String amountorder) { this.amountorder = amountorder; }
    public String getAmountorder() { return amountorder; }
}


