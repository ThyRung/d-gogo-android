package com.thyrung.firebase;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.thyrung.firebase.model.TranItem;

import java.util.ArrayList;
import java.util.List;


public class Transaction extends AppCompatActivity {
    private List<TranItem> list;
    private TransactionAdapter transactionAdapter;
    private RecyclerView recyclerView;
    private ImageButton imageTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transaction);
        imageTransaction = findViewById(R.id.cancel_transation);
        imageTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recyclerView = findViewById(R.id.recycler_view);
        list = new ArrayList<TranItem>();

        for (int i =0; i<7; i++){
            list.add(new TranItem("15.00%","Monday June, 2020 9:00 AM","GMT+07:00","-5,000.00"));
        }

        transactionAdapter = new TransactionAdapter(list, Transaction.this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(transactionAdapter);
    }

}
