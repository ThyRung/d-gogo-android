package com.thyrung.firebase;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;

public class Channels extends AppCompatActivity {
    private Switch switch1,switch2;
    private ImageButton imgClose;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.channels);
        switch1 = findViewById(R.id.id_switchone);
        switch2 = findViewById(R.id.id_switchtwo);

        imgClose = findViewById(R.id.id_toolbar);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    public void onSwitchClick(View v){
        if(switch1.isChecked()){
            Toast.makeText(this.switch1.getContext(),"Toggle ON",Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this.switch1.getContext(),"Toggle OFF",Toast.LENGTH_SHORT).show();
        }
        if(switch2.isChecked()){
            Toast.makeText(this.switch2.getContext(),"Toggle ON",Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this.switch2.getContext(),"Toggle OFF",Toast.LENGTH_SHORT).show();
        }
    }
}
