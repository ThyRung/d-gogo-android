package com.thyrung.firebase;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.thyrung.firebase.model.DataItem;
import com.thyrung.firebase.model.TranItem;

import java.text.BreakIterator;
import java.util.List;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder> {
    private final List<TranItem> mlist;
    private Context context;
    public TransactionAdapter(List<TranItem> list, android.content.Context context){
        this.mlist = list;
        this.context = context;
    }

    @NonNull
    @Override
    public TransactionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_transaction,parent,false);
        return new TransactionAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionAdapter.ViewHolder holder, int position) {
        final TranItem item = mlist.get(position);

        holder.percentorder.setText(item.getpercentorder());
        holder.datetimer.setText(item.getDatetimer());
        holder.timeorder.setText(item.getTimeorder());
        holder.amountorder.setText(item.getAmountorder());

    }

    @Override
    public int getItemCount() { return  mlist.size(); }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView percentorder,datetimer,timeorder,amountorder;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            percentorder = itemView.findViewById(R.id.percentorder);
            datetimer = itemView.findViewById(R.id.datetimer);
            timeorder = itemView.findViewById(R.id.timeorder);
            amountorder = itemView.findViewById(R.id.amountorder);
        }
    }
}
