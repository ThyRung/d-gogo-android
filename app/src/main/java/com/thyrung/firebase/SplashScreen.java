package com.thyrung.firebase;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

//        if you login ready but no login again, it's auto Login.
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        final Intent home = new Intent(SplashScreen.this,MainActivity.class);
        final Intent login = new Intent(SplashScreen.this,LoginActivity.class);

        new Handler().postDelayed(new Runnable() {
            // Using handler with postDelayed called runnable run method
            @Override
            public void run() {
                if (firebaseUser!=null){
                    startActivity(home);
                } else {
                    startActivity(login);
                }
                // close this activity
                finish();
            }
        },3 *1000);
    }
}
