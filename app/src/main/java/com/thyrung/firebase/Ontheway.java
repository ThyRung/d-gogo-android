package com.thyrung.firebase;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

public class Ontheway extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.on_the_way);
        findViewById(R.id.btn_pickup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               ShowDialogArrived();
            }

            private void ShowDialogArrived() {
                AlertDialog.Builder builder = new AlertDialog.Builder(Ontheway.this, R.style.AlertDailogTheme);
                View view = LayoutInflater.from(Ontheway.this).inflate(
                        R.layout.dailog_ontheway,(ConstraintLayout)findViewById(R.id.layoutDialogTheWay)
                );
                builder.setView(view);
                ((Button)view.findViewById(R.id.btn_Arrived)).setText(getResources().getString(R.string.dialog_Arrived));

                final AlertDialog alertDialog = builder.create();

                view.findViewById(R.id.btn_Arrived).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(getApplicationContext(),Paidwithcash.class));
                    }
                });
                if(alertDialog.getWindow()!= null){
                    alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                }
                alertDialog.show();
            }
        });
        //findViewById(R.id.btn_Arrived).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getApplicationContext(),Paidwithcash.class));
//            }
//        });
//
    }
}
