package com.thyrung.firebase;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.thyrung.firebase.service.FirebaseService;

public class ProfileX extends AppCompatActivity {

    private LinearLayout linearLayoutPersonProfile;
    private CircularImageView circularImageView;
    private ImageButton imgCamera;
    private TextView tvId,tvName, tvPhone, tvCity;

    private static final int PICK_IMAGE = 100;
    private Uri imageUri;
    private CircularImageView imgGallery;
    private Button btnLogout;

    private FirebaseUser firebaseUser;
    private FirebaseFirestore firebaseFirestore;
    private StorageReference mStorageRef;
    private String profileId;
    private DatabaseReference reference;

    private BottomSheetDialog bottomSheetDialog, bsDialogEditName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_x);

        tvId = findViewById(R.id.mId);
        tvName = findViewById(R.id.userName);
        tvPhone = findViewById(R.id.mId);
        tvCity = findViewById(R.id.mCity);

        imgGallery = findViewById(R.id.image_profile);

        btnLogout = findViewById(R.id.profile_logout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogSignOut();
            }
        });

        imgCamera = findViewById(R.id.img_camera_profile);
        imgCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openGallery();
            }
        });

        circularImageView = findViewById(R.id.image_profile);
        circularImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                circularImageView.invalidate();
                Drawable drawable = circularImageView.getDrawable();
//                Common.IMAGE_BITMAP = ((GlideBitmapDrawable)drawable.getCurrent()).getBitmap();
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(ProfileX.this,
                        circularImageView, ViewCompat.getTransitionName(circularImageView));

                startActivity(new Intent(getApplicationContext(), ViewImageX.class), options.toBundle());

            }
        });

        linearLayoutPersonProfile = findViewById(R.id.profile_person);
        linearLayoutPersonProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
//                Toast.makeText(getApplicationContext(),"HAHAHA",Toast.LENGTH_SHORT).show();

                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(ProfileX.this,
                        linearLayoutPersonProfile, ViewCompat.getTransitionName(linearLayoutPersonProfile));

                startActivity(new Intent(getApplicationContext(), MainActivity.class), options.toBundle());
            }
        });

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        mStorageRef = FirebaseStorage.getInstance().getReference();

        if (firebaseUser != null) {
            profileId = firebaseUser.getUid();
            reference = FirebaseDatabase.getInstance().getReference().child("User").child(profileId);
        }
    }

    //    Connection camera & gallery
    private void openGallery() {

        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }

    //    Update image to firebase
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE){

            imageUri = data.getData();
            imgGallery.setImageURI(imageUri);

            new FirebaseService(ProfileX.this).uploadImageToFireBaseStorage(imageUri, new FirebaseService.OnCallBack() {
                @Override
                public void onUploadSuccess(String imageUrl) {

                    Log.d("TAG", "onUploadSuccess: url "+imageUrl);
                }

                @Override
                public void onUploadFailed(Exception e) {

                    Log.e("TAG", "onUploadFailed: "+e.getMessage());
                }
            });
        }
    }


    private void showDialogSignOut(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(ProfileX.this);
        builder.setMessage("Do you want to sign out?");
        builder.setPositiveButton("Sign out", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(ProfileX.this, LoginActivity.class));
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}