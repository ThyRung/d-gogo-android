package com.thyrung.firebase;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

public class NewJob extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newjob);
        findViewById(R.id.btn_Accept).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),Ontheway.class));
            }
        });
        findViewById(R.id.Reject).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDailogAccpt();
            }

            private void ShowDailogAccpt() {
                AlertDialog.Builder builder = new AlertDialog.Builder(NewJob.this, R.style.AlertDailogTheme);
                View view = LayoutInflater.from(NewJob.this).inflate(
                        R.layout.dailog_accept,(ConstraintLayout)findViewById(R.id.layoutDialogAccept)
                );
                builder.setView(view);
                ((TextView)view.findViewById(R.id.NameAccount)).setText(getResources().getString(R.string.dialog_Accept_Name));
                ((TextView)view.findViewById(R.id.PhoneACustomer)).setText(getResources().getString(R.string.dialog_Accept_Phone));
                final AlertDialog alertDialog = builder.create();
                if(alertDialog.getWindow()!= null){
                    alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                }
                alertDialog.show();
            }
        });

    }
}
