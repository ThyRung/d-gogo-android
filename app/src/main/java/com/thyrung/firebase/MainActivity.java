package com.thyrung.firebase;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.material.navigation.NavigationView;
import com.mikhaellopez.circularimageview.CircularImageView;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String TAG = "MainActivity";
    private static final int ERROR_DIALOG_REQUEST = 901;

    private Switch aSwitch;
    private ImageView imgOffOn;
    private Button btnChannels, btnPreOrder;

    private DrawerLayout mDrawerLayout;
    private ImageButton imgMenu;

    private LinearLayout lCredit, lBilling, lChannels, lRecent, lGetToTouch;
    private LinearLayout linearLayoutProfile;
    private ImageView imgProfile;
    private TextView tvId,tvName,tvNumber,tvCity;
    private CircularImageView ciProfile;

//    private SearchView mSearchView;

//  Vars
    private boolean mLocationPermissionGranted = false;
    private GoogleMap gMap;


    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        mSearchView = findViewById(R.id.search_location);
//        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String s) {
//                String location = mSearchView.getQuery().toString();
//                List<Address> addressList = null;
//
//                if (location != null || location.equals("")){
//                    Geocoder geocoder = new Geocoder(MainActivity.this);
//                    try {
//                        addressList = geocoder.getFromLocationName(location, 1);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//
//                    Address address = addressList.get(0);
//                    LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
//                    gMap.addMarker(new MarkerOptions().position(latLng).title(location));
//                    gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
//                }
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String s) {
//                return false;
//            }
//        });


//      Get Location Permission
        getLocationPermission();

//      Down value GoogleMaps
        initMap();

//   Click Drawer || Navigation
        mDrawerLayout = findViewById(R.id.drawer);
        imgMenu = findViewById(R.id.btnMenu);
        imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        NavigationView navigationView = findViewById(R.id.id_nav);
        View header = navigationView.getHeaderView(0);


        // navigationView.getHeaderView(0);
        imgProfile = findViewById(R.id.image_profile);
        tvId = findViewById(R.id.mId);
        tvName = findViewById(R.id.userName);
        tvNumber = findViewById(R.id.mPhone);
        tvCity = findViewById(R.id.mCity);
        linearLayoutProfile = header.findViewById(R.id.header);
        linearLayoutProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getApplicationContext(),"HAHAHA",Toast.LENGTH_SHORT).show();

                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this,
                        linearLayoutProfile, ViewCompat.getTransitionName(linearLayoutProfile));

                startActivity(new Intent(getApplicationContext(), ProfileX.class), options.toBundle());
            }
        });

        lCredit = header.findViewById(R.id.card_Credit);
        lCredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getApplicationContext(),"HAHAHA",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),Credit.class));
            }
        });

        lBilling = header.findViewById(R.id.card_Billing);
        lBilling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getApplicationContext(),"HAHAHA",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),BillingPlan.class));
            }
        });

        lChannels = header.findViewById(R.id.card_Channels);
        lChannels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getApplicationContext(),"HAHAHA",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),Channels.class));
            }
        });

        lRecent = header.findViewById(R.id.card_Recent);
        lRecent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getApplicationContext(),"HAHAHA",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),RecentJobs .class));
            }
        });

        lGetToTouch = header.findViewById(R.id.card_Get_in_touch);
        lGetToTouch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getApplicationContext(),"HAHAHA",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),Transaction.class));
            }
        });

        btnChannels = findViewById(R.id.btn_channels);
        btnChannels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(),Channels.class));
            }
        });

        imgOffOn = findViewById(R.id.img_OffOn);
        imgOffOn.setImageDrawable(getResources().getDrawable(R.drawable.light_off));

        aSwitch = findViewById(R.id.id_Switch);
        aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (aSwitch.isChecked()) {
                    imgOffOn.setImageDrawable(getResources().getDrawable(R.drawable.light_on));
                } else {
                    imgOffOn.setImageDrawable(getResources().getDrawable(R.drawable.light_off));
                }
            }
        });
    }

//    private void init(){
//        Log.d(TAG, "init: initializing");
//
//        mSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
//                if (i == EditorInfo.IME_ACTION_SEARCH
//                        || i == EditorInfo.IME_ACTION_DONE
//                        || keyEvent.getAction() == keyEvent.ACTION_DOWN
//                        || keyEvent.getAction() == keyEvent.KEYCODE_ENTER){
//
////                    execute our method for searching
//                    getLocate();
//                }
//                return false;
//            }
//        });
//    }

//    private void getLocate(){
//
//        Log.d(TAG, "getLocate: getLocating");
//        String searchLocate = mSearch.getText().toString();
//        Geocoder geocoder = new Geocoder(MainActivity.this);
//        List<Address> list = new ArrayList<>();
//
//        try {
//
//            list = geocoder.getFromLocationName(searchLocate, 1);
//        }catch (IOException e){
//
//            Log.d(TAG, "getLocate: IOException: " + e.getMessage());
//        }
//        if (list.size() > 0){
//
//            Address address = list.get(0);
//
//
////            Toast.makeText(this, address.toString(), Toast.LENGTH_SHORT).show();
//        }
//
//    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Toast.makeText(this, "Map is Ready", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onMapReady: map is ready");
        gMap = googleMap;
    }

//  Show GoogleMaps
    private void initMap() {
        Log.d(TAG, "initMap: initializing map");
        // SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map).;
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                gMap = googleMap;
                Log.d(TAG, "onMapReady: YES ");
                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                gMap.setMyLocationEnabled(true);
                gMap.getUiSettings().setMyLocationButtonEnabled(true);
            }
        });
    }

//  Select GoogleMaps
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG,"onRequestPermissionsResult: called");
        mLocationPermissionGranted = false;

        switch (requestCode){
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0){
                    for (int i = 0; i < grantResults.length ; i++){
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED){
                            mLocationPermissionGranted = false;
                            Log.d(TAG,"onRequestPermissionsResult: permission failed");
                            return;
                        }
                    }
                    Log.d(TAG,"onRequestPermissionsResult: permission granted");
                    mLocationPermissionGranted = true;
                    // initialize our map
//                    initMap();
                }
            }
        }
    }

    private void getLocationPermission(){
        Log.d(TAG, "getLocationPermission: getting location permission");
        String [] permission = {
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
        };

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                FINE_LOCATION ) == PackageManager.PERMISSION_GRANTED){
            if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                mLocationPermissionGranted = true;
                initMap();
            }else {
                ActivityCompat.requestPermissions(this,permission,LOCATION_PERMISSION_REQUEST_CODE);
            }
        }else {
            ActivityCompat.requestPermissions(this,permission,LOCATION_PERMISSION_REQUEST_CODE);
        }

    }
}
