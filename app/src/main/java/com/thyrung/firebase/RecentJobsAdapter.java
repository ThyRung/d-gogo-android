package com.thyrung.firebase;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecentJobsAdapter extends RecyclerView.Adapter<RecentJobsAdapter.ViewHolder> {
    private List<RecentItem> list;
    private Context context;
    public RecentJobsAdapter(List<RecentItem> list, Context context){
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public RecentJobsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_recent_jobs,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecentJobsAdapter.ViewHolder holder, int position) {
        final RecentItem item = list.get(position);
        holder.day.setText(item.getDay());
            holder.month.setText(item.getMonth());
            holder.year.setText(item.getYear());
            holder.time.setText(item.getTime());
            holder.total.setText(item.getTotal());
            holder.icon1.setImageDrawable(item.getIcon1());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView icon1,icon2;
        private TextView day,month,year,time,total;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            icon1 = itemView.findViewById(R.id.id_icon1);
            day = itemView.findViewById(R.id.id_day);
            month = itemView.findViewById(R.id.id_month);
            year = itemView.findViewById(R.id.id_year);
            time = itemView.findViewById(R.id.id_time);
            total = itemView.findViewById(R.id.id_total);
        }
    }
}
